<?php
$currencies = [
	"AUD",
	"RUB",
	"JPY",
	"USD",
	"EUR",
	"CNY",
];
$api_url = "http://api.fixer.io/latest";
if(!empty($_POST['base_currency']) and !empty($_POST['currency_list'])) {
	if(in_array($_POST['base_currency'], $currencies)) {
		$params_array = array (
			'base' => $_POST['base_currency'],
			'symbols' => $_POST['currency_list']
		);
		$request_url = $api_url . "?" . http_build_query($params_array);
	echo $request_url;
	}
}

//$result = file_get_contents($request_url);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Currency</title>

	<!-- Bootstrap -->
	<link href="<?=PUBLIC_DIR?>/css/bootstrap.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<?php
require_once "template_head.php";
?>

<div class="container">

	<div class="starter-template">
		<h1>Bootstrap starter template</h1>
		<p class="lead">Currency.<br> All you get is this text and a mostly barebones HTML document.</p>
		</div>
<div>
	<form method="post">
		<p>Base currency:</p>
		<select class="form-control" name="base_currency">
			<?php
foreach ($currencies as $currency) {
	echo "<option value='{$currency}' > {$currency}</option>";
}
?>
		</select>
<p> Currencies list</p>
		<?php
		foreach ($currencies as $currency) {
			echo "<input type='checkbox' name='currency_list[]' value='{$currency}'>{$currency}<br>";
		}
		?>
<input type="submit" value="Отправить">



	</form>




</div>
</div><!-- /.container -->

<?php
require_once "template_footer.php";  //bootstrap and jquery
?>
</body>
</html>
